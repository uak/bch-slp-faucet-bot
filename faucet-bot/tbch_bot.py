#!/usr/bin/env python
# pylint: disable=W0613, C0116
# type: ignore[union-attr]

# This program is published under the AGPL v3 license
# © 2021 gitlab.com/uak

# To do
# 1. Check that there is token balance before starting bot
# 2. Check that bot slp and bch wallets are loaded

# replaced notice from conversation handler bot

"""
BCH testnet coins faucet bot, you can setup this bot to hand people
test net coins.

Usage:
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import os
import os.path
import sys
import logging
import configparser

from sys import exit
from decimal import Decimal
from datetime import datetime, timedelta

from ec_slp_lib import EcSLP

# Telegram bot imports
from telegram import (
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    Update,
    ParseMode,
)
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)


from models import save_transaction, recent_user

home_dir = os.path.expanduser('~')

faucet_conf = os.environ["faucet_conf"]

home_dir = os.path.expanduser('~')




config_dict={
    "faucet_conf":"",
    "allow_zero_balance":"",
    "token_id_hex":"",
    "donate":"",
    "donate_tbch":"",
    "tbch_amount":"",
    "token_amount":"",
    "min_allowed_balance":"",
    "explorer":"",
    "slp_explorer":"",
    "time_gap":"",
    "slp_explorer":"",
    "data_dir":"",
    "token":""
    }

params={
    "rpc_user": "",
    "rpc_url": "",
    "rpc_port": "",
    "rpc_password": "",
    "network": "",
    "python_path": "",
    "electron_cash_path": "",
    "wallet_path": "",
    "use_custom_dir": "",
    "custom_dir": "",
}

if os.path.isfile(faucet_conf):
    config = configparser.ConfigParser()
    config.read(faucet_conf)
else:
    exit("No tbch config file")

for variable in config_dict:
    config_dict[variable] = os.getenv(variable, config['settings'].get(variable, None))

for variable in params:
    if variable == "use_custom_dir":
        params["use_custom_dir"] = config_dict[variable] = os.getenv(variable, config['ec_slp_lip'].getboolean(variable, None))
    else:
        params[variable] = os.getenv(variable, config['ec_slp_lip'].get(variable, None))
    

# Get configurations from environment variables, useful for docker
faucet_conf = config_dict["faucet_conf"]
wallet_path = params["wallet_path"]
allow_zero_balance = config_dict["allow_zero_balance"]
data_dir=config_dict["data_dir"]
token_id_hex = config_dict["token_id_hex"]
donate = config_dict["donate"]
donate_tbch = config_dict["donate_tbch"]
tbch_amount = config_dict["tbch_amount"]
token_amount = config_dict["token_amount"]
token = config_dict["token"]
min_allowed_balance =  config_dict["min_allowed_balance"]
explorer = config_dict["explorer"]
slp_explorer = config_dict["slp_explorer"]
time_gap = config["database"]["time_gap"]

ec_slp = EcSLP(params)

# Check Electron Cash daemon running
daemon_running = ec_slp.check_daemon()

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,  # DEBUG #INFO
)

logger = logging.getLogger(__name__)


if not daemon_running:
    exit("daemon is not running") 

# Enable logging


# Check if config file exists 
config_file = "tbch_bot.config"

logger.info(f"Faucet configuration file: {faucet_conf}")


# check if the wallet is loaded, exit if not
wallet_loaded_bch = ec_slp.check_wallet_loaded(wallet_path)

if not wallet_loaded_bch:
    exit("BCH wallet is not loaded") # Exit python if wallet not loaded
logger.info(f"Wallet loaded: {wallet_path}")


#check wallet token balance before start, if balance lower than minimum
# limit do not start.
bch_balance = ec_slp.get_bch_balance()

# sum the balance of confirmed and unconfirmed if there is an unconfirmed balance
if "unconfirmed" in bch_balance and Decimal(bch_balance["unconfirmed"]) > 0:
    balance = Decimal(bch_balance["confirmed"]+bch_balance["unconfirmed"])
else:
    balance = Decimal(bch_balance["confirmed"])


# Exit if balance is lower than minimum allowed
if Decimal(balance) >= Decimal(min_allowed_balance):
    logger.info(f"Balance is: {balance}")
    pass
else:
    logger.info(f"No token balance: {balance}")
    # Exit python if wallet not loaded
    exit()


# Those are by Telegram bot
CONFIRM, INFORMATION = range(2)


# Bot first message asking for token amount to buy
def start(update: Update, context: CallbackContext) -> int:
    """Start the bot
    Offer user to receive test bch coins or test slp coins
    """
    # Get bch balance
    balance = Decimal(bch_balance["confirmed"])
    # Check if there is enough balance in the wallet
    if Decimal(balance) > Decimal(min_allowed_balance):
        text = (
            "Hi, I can give you BCH test coins (tBCH) "
            "Please send your bchtest address to get tBCH.\n"
            "You can get a test address by using "
            '<a href="https://electroncash.org/">Electron Cash</a> '
            "wallet with <code>--testnet</code> flag, "
            'for support check our <a href="https://t.me/slpsell_bot">'
            "SLP bot group</a> \n"
            'If you want sTST SLP tokens instead send a slptest address\n'
            "⚠️ This bot is still under development use it at your own risk. \n"
            "Send /cancel to stop."
        )
        update.message.reply_text(text, parse_mode="html")
    else:
        text = (
            "Sorry, not enough tBCH in the faucet, please contact us"
            'on our <a href="https://t.me/slpsell_bot">SLP bot group</a>\n'
            "You can also donate to the address in the bot /info"
        )       
        update.message.reply_text(text, parse_mode="html")

    return CONFIRM


def prepare_broadcast_bch():
    # Prepare transaction using EC SLP lib
    # amount from tbch_amount value
    
    global tx_id

    amount = tbch_amount
    tx_hex = ec_slp.prepare_bch_transaction(
        recipient_address,
        amount,
    )
    
    # Brodacast transaction using EC SLP lib
    
    tx_id = ec_slp.broadcast_tx(tx_hex["hex"])
    logger.info(f"Raw transaction: {tx_hex['hex']}")
    return tx_id # use := operator in future

def prepare_broadcast_slp():
    # Prepare transaction using EC SLP lib
    amount = token_amount
    tx_hex = ec_slp.prepare_slp_transaction(
        token_id_hex,
        recipient_address,
        amount,
    )
    
    # Brodacast transaction using EC SLP lib
    global tx_id
    tx_id = ec_slp.broadcast_tx(tx_hex["hex"])
    logger.info(f"Raw transaction: {tx_hex['hex']}")
    return tx_id # use := operator in future


def confirm(update: Update, context: CallbackContext) -> int:
    """
    Send the coin to user when they send an address
    """
    try:
        address = update.message.text
    except:
        logger.info("No text in message.")
    global recipient_address

    # do basic validation of user address, send if valid
    if address.startswith("bchtest") and len(address) == 50 and ec_slp.validate_address(address):
        coin_type = "bch"
        # check if user have asked for coins recently
        if not recent_user(update.message.from_user.id, coin_type):
            transaction_status = 1
            recipient_address = update.message.text
            # Broadcast transaction using Electron Cash wallet
            tx_id = prepare_broadcast_bch()
            amount = tbch_amount
        else:
            text = (
                    "You have already requested tBCH in the last"
                    f" {time_gap} hours, You can ask later "
                    )
            update.message.reply_text(text,
                                parse_mode="html") 
            return CONFIRM

    # do basic validation of user address, send if valid
    elif address.startswith("slptest") and len(address) == 50 and ec_slp.validate_address(address):
        coin_type = "slp"
        transaction_status = 1
        recipient_address = update.message.text
        # Broadcast transaction using Electron Cash wallet
        tx_id = prepare_broadcast_slp()
        amount = token_amount
    else:
        update.message.reply_text("the address is not valid", parse_mode="html")  
        return CONFIRM
        

    # log user address
    logger.info(f"user address is: {update.message.text}")
    text = (
        f"You should have received {amount} {coin_type} to your address.\n"
        f'Transaction ID is: <a href="{explorer}{tx_id[1]}">{tx_id[1]}</a> \n'
        "send /start when you want more. Thank you."
        )
    update.message.reply_text(text, parse_mode="html")        
    tg_user_id = update.message.from_user.id
    tg_user_name = update.message.from_user.first_name

    # save data to the database
    save_transaction(
                tg_user_id,
                tg_user_name,
                recipient_address,
                coin_type,
                amount,
                transaction_status,
                tx_id
                )
    logger.info(f"Transaction completed, ID: {tx_id[1]}")
    return ConversationHandler.END
 
def test1(update: Update, context: CallbackContext) -> int:
    """"print stuff when command sent"""
    update.message.reply_text("testy")
    return information

def information(update: Update, context: CallbackContext) -> int:
    """
    Print useful information about the bot
    """
    update.message.reply_text(
        "Thank you for your business! If you want"
         " to incourage developement of this bot and"
         f" tip the dev plase donate to this campagin: {donate}"
    )
    logger.info("User asked for info")

    return ConversationHandler.END


# Canceling conversation
def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info(f"User {user.first_name} canceled the conversation.")
    update.message.reply_text(
        "Bye! I hope we can serve you again some day.",
        reply_markup=ReplyKeyboardRemove(),
    )

    return ConversationHandler.END


def main() -> None:
    # Create the Updater and pass it your bot's token.
    updater = Updater(token)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            CONFIRM: [
                MessageHandler(Filters.text & ~Filters.command, confirm)
            ],
            INFORMATION: [
                MessageHandler(Filters.text & ~Filters.command, information)
            ],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    msg_handler = CommandHandler(["info","information"], information)
    dispatcher.add_handler(conv_handler)
    dispatcher.add_handler(msg_handler)
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == "__main__":
    main()
